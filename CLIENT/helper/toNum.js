export default function toNum(str){
	//convert string to array, to gain access to array methods
	const arr = [...str]
	// console.log(arr)
	const filteredArr = arr.filter(element => element !== ",")
	return parseInt(filteredArr.reduce((x,y)=> x + y))
	// console.log(filteredArr)
	// console.log(parseInt(filteredArr.reduce((x,y)=> x + y)))

}