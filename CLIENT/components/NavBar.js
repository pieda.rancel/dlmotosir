import { useContext } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar() {
    //access the global user state via the UserContext
    const { user } = useContext(UserContext)

    return (
        <Navbar expand="lg" id="nav">
            <Link href="/">
                <a className="navbar-brand text-white">Budget Tracker</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">

                    {(user.id !== null)
                        ? <>
                            <Link href="/categories/addcategory">
                                <a className="nav-link text-white" role="button">Add Category</a>
                            </Link>
                            <Link href="/records/addrecord">
                                <a className="nav-link text-white" role="button">Add Record</a>
                            </Link>
                           
                            <Link href="/categories">
                                <a className="nav-link text-white" role="button">Categories</a>
                            </Link>
                            <Link href="/records">
                                <a className="nav-link text-white" role="button">Records</a>
                            </Link>
                            <Link href="/chart">
                                <a className="nav-link text-white" role="button">Chart</a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link text-white" role="button">Logout</a>
                            </Link>
                            
                       	  </>
                    	: <>
	                        <Link href="/login">
	                            <a className="nav-link text-white" role="button">Login</a>
	                        </Link>
	                        <Link href="/register">
	                            <a className="nav-link text-white" role="button">Register</a>
	                        </Link>
	                      </>
                }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
