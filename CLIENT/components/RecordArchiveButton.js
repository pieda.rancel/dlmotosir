import {Button, Tooltip} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import Router from 'next/router'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'

export default function recordArchiveButton({recordId, isActive}) {
    const { user, balance, setBalance } = useContext(UserContext)
    const archive = (recordId) => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/records/${recordId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => { 
            if(data === false){
                Router.push('/error')
            }else{
                console.log(data)            
                Router.reload()
            }
        })
    }
    if(isActive === true){
    	return  (
                <Tooltip title="Archive" placement="right">
                    <Button variant="contained" color="secondary" onClick={()=>archive(recordId)}>
                      <DeleteIcon/> 
                    </Button>
                </Tooltip>
        )
    }else{
    	return  <Button variant="contained" color="primary" onClick={()=>archive(recordId)}>Reactivate</Button>
    

    }
    
}
