import {Card} from 'react-bootstrap'
import RecordArchiveButton from './RecordArchiveButton'
import {useContext} from 'react'
import UserContext from '../UserContext'



export default function RecordTable({recordData}) { 
	const { user, balance } = useContext(UserContext)
	// console.log(balance)
 //   	console.log(recordData)

    return (
    	<>
				<tr>
				    <td key={recordData._id}>{recordData.description}</td>
				    <td> {recordData.categoryName}</td>
				    <td>  {recordData.categoryType}</td>
				    <td>{(recordData.categoryType==="income") ? " +" :" -"}{recordData.amount}</td>
				    <td> {recordData.runningBalance}</td>
				    <td> {recordData.createdOn}</td>
				    <td> {user.balance}</td>
				    <td> <RecordArchiveButton recordId={recordData._id} isActive={recordData.isActive}/></td>
		      	</tr>
			      
		      	

    	</>	
    )
}