import {Doughnut} from 'react-chartjs-2'
import {Button} from '@material-ui/core';
import {useState} from 'react'

export default function DoughnutChart({totalIncome, totalExpense, allrecords}){
	const [ records, setRecords ] = useState(false)
	const handledClick = ()=>{
		setRecords(!records)
	}
	const colorList = ["green", "red", "yellow", "orange", "blue", "pink", "violet", "purple","AliceBlue",
	"AntiqueWhite","Aqua","Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown"];
	return(
	<>
		<Button variant="contained" color="primary" onClick={handledClick}>{records ? "Show all records" : "Show Totals "}</Button>
		<Doughnut
			data = {{
				datasets:[
					{	
						data: records ? allrecords.map(record => (record.amount)) : [totalIncome, totalExpense],
						backgroundColor: colorList
					}
				],
				labels: records ? allrecords.map(record=>record.categoryName) : ["Total Income", "Total Expense"]
			}}

			redraw={false}

		/>
		
	</>
		)
}








