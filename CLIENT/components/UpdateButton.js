import UpdateIcon from '@material-ui/icons/Update';
import {Button, Tooltip} from '@material-ui/core';
import Router from 'next/router'

export default function UpdateButton({categoryId}) {
	console.log(categoryId)
    const dirToEditForm = (categoryId) => {
        Router.push({
            pathname: '/categories/edit',
            query: {categoryId: categoryId}
        })
    }
    return (
        <Tooltip title="Update" placement="left">
            <Button variant="contained" color="primary" onClick={() => dirToEditForm(categoryId)}>
            <UpdateIcon/>
            </Button>
        </Tooltip>
    )
}