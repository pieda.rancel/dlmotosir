import Router from 'next/router'
import {Button, Tooltip} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';




export default function ArchiveButton({categoryId, isActive}) {

    const archive = (categoryId) => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/${categoryId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Router.reload()
            }else{
                Router.push('/error')
            }
        })
    }
    if(isActive === true){
    	return  (
                <>
                <Tooltip title="Delete" placement="right" arrow>
                 <Button variant="contained" color="secondary"  onClick={()=>archive(categoryId)}>
                 <DeleteIcon/>
                </Button>
                 </Tooltip>
                </> 
                 
                 )
    
    }else{
    	return  <Button variant="contained" color="primary" onClick={()=>archive(categoryId)}>Reactivate</Button>
    

    }
    
}
