import { useState, useEffect } from 'react'
import { Form, Alert, Spinner, Container, Row, Col } from 'react-bootstrap'
import {Button} from '@material-ui/core'
import UpdateIcon from '@material-ui/icons/Update';
import Router from 'next/router'
import { useRouter } from 'next/router'

export default function edit() {
    const router = useRouter()
    const { categoryId } = router.query
    const [name, setName] = useState('')
    const [type, setType] = useState('')
    const [isActive, setIsActive] = useState(false)
    const [notify, setNotify] = useState(false)
    console.log(categoryId)
    //retrieve category information on component getting mounted
    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/${categoryId}`)
        .then(res => res.json())
        .then(data => {
    console.log(data.name, data.type)
            setName(data.name)
            setType(data.type)
        })
    }, [])

    useEffect(() => {
        if(name.length < 50){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [name])

    function editCategory(e){
        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/${categoryId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                type:type
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data===true){
                Router.push('/categories')
            }else{
                setNotify(true)
            }
        })
    }

    return (
        <>
        <Container fluid>
            <Row>
                <Col xs={12} id="add-record-first-layer">
                    <Row>
                        <Col xs={12} md={{ span: 6, offset: 3 }}   id="add-record-second-layer">
                            {name===''
                            ? <><h1>Retrieving category info...</h1><Spinner /></>
                            : <Form onSubmit={(e) => editCategory(e)}>
                                <Form.Group>
                                    <Form.Label className="text-white">Category Name:</Form.Label>
                                    <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                                    {name.length >= 50 ? <Alert variant="warning">Name has exceeded maximum length</Alert>:null}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label className="text-white">Category Type:</Form.Label>
                                    <Form.Control as="textarea" rows="3" value={type} onChange={e => setType(e.target.value)} required />
                                </Form.Group>
                                {isActive===true
                                ? <Button type="submit" variant="contained" color="primary">
                                    <UpdateIcon/>
                                    Update
                                 </Button>
                                : <Button disabled type="submit" variant="contained">
                                    <UpdateIcon/>
                                    Update
                                    </Button>}
                            </Form>}
                            

                            {notify === true
                            ? <Alert variant="danger">Failed to update Category!</Alert>
                            : null}
                        </Col>
                    </Row>
                </Col>
            </Row>    
        </Container>
        </>

    )
}
