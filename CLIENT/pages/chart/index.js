import { useState, useEffect, useContext } from 'react'
import { Jumbotron, Row, Col, Container, Table  } from 'react-bootstrap'
import UserContext from '../../UserContext'
import DoughnutChart from '../../components/DoughnutChart'



export async function getServerSideProps() { 
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/records`)
    const data = await res.json()

    //use the pre-fetched data as a prop to this component that will be pre-rendered
    return {
        props: {
            data
        }
    }
}


export default function index({data}) {

    const { user } = useContext(UserContext)
    const recordTypeIncome = data.filter(record => record.categoryType === "income")
    const recordTypeExpense = data.filter(record => record.categoryType === "expense")
    console.log(recordTypeIncome)
    const recordMapIncome =  recordTypeIncome.map(record => record.amount)
    const recordMapExpense =  recordTypeExpense.map(record => record.amount)
    const recordTotalIncome = recordMapIncome.reduce((x,y)=> {return x+y},0)
    const recordTotalExpense = recordMapExpense.reduce((x,y)=> {return x+y},0)
    console.log(recordTotalIncome, recordTotalExpense)
return( 
    <DoughnutChart allrecords={data} totalIncome={recordTotalIncome} totalExpense={recordTotalExpense}/>
    )

}



