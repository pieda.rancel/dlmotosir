import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
//import the context provider
import { UserProvider } from '../UserContext'
import { Container } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Head from 'next/head'
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

function MyApp({ Component, pageProps }) {
  //initialize a globally available user state as an object with id and isAdmin properties both set to null
  const [user, setUser] = useState({
    id: null
  })



  const [records, setRecords] = useState([])
  const [balance, setBalance] = useState(0)
  const [categories, setCategories] = useState([])





  //function for clearing local storage upon logout
  const unsetUser = () => {
    //removes the JWT that was stored in the local storage of app
    localStorage.clear()
    //set the global user state properties to null
    setUser({
      id: null
    })
    setBalance(0)
  }

  //effect hook that will get user details from the API whenever the user state's id property changes
  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, { 
      headers: {
        //the user info that will be retrieved from the API depends on the value of the encoded JWT
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    //parse the response into desired file type
    .then(res => res.json())
    .then(data => {

      //if JWT is valid set user id and isAdmin states accordingly
        if(data._id){
          setUser({
            id: data._id 
          })
          fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${data._id}/records`, {
            headers: {
              Authorization: "Bearer ${localStorage.getItem('token')}"
            }
          })
          .then(res => res.json())
          .then(data => {
            console.log(data)
            setRecords(data)
            const computeBalance = data.reduce((x, y)=> { 
              if(y.categoryType = "income"){
                return +x + +y.amount
              }else{
                return +x - +y.amount
              }
            }, 0)
            setBalance(computeBalance)
            fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/`, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
              }
            })
            .then(res => res.json())
            .then(data => {
              console.log(data)
                setCategories(data)
            })
          })

        }else{
          setUser({
            id: null
          })
          setBalance(0)
        } 
           
      })
    }, [user.id])

  return (
    // make the user state, setUser state setter, and unsetUser function globally available in our app by passing them in as values to the context provider
    <>
    <Head>
      <title>Budget Tracker</title>
    </Head>
    {/* context providers have to wrap the entire application's component tree so that any component in the tree can subscribe to changes in the global values being provided */}
    <UserProvider value={{user, setUser, categories, setCategories, unsetUser, balance, setBalance, records, setRecords}}>
      <NavBar />
        <Component {...pageProps} />
    </UserProvider>
    </>
  )
}

export default MyApp
