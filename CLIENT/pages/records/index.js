import { useState, useEffect, useContext } from 'react'
import { Jumbotron, Row, Col, Container, Table  } from 'react-bootstrap'
import RecordTable from '../../components/RecordTable'
import UserContext from '../../UserContext'



export async function getServerSideProps() { 
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/records`)
    const data = await res.json()

    //use the pre-fetched data as a prop to this component that will be pre-rendered
    return {
        props: {
            data
        }
    }
}


export default function index({data}) {
    const { user, balance } = useContext(UserContext)
    const [activeRecords, setActiveRecords] = useState([])

    console.log(balance)
    useEffect(() => {
        if(data.length > 0){
            setActiveRecords(data.filter(record => record.isActive === true))
        }
    }, [data])
    
    if(data.length === 0){
        return (
            <Jumbotron>
                <h1>There are no available records yet</h1>
            </Jumbotron>
        )
    }else{
        if(activeRecords.length > 0){
            return ( 
                        <Container fluid>
                            <Row>
                                <Col xs={12} id="record-first-layer"> 
                                    <Row>
                                        <Col xs={12} md={{ span: 10, offset: 1 }} id="record-second-layer">
                                            <Table striped bordered hover size="lg" responsive="md">
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Category Name</th>
                                                    <th>Category Type</th>
                                                    <th>Amount</th>
                                                    <th>Running Balance</th>
                                                    <th>Date Added</th>
                                                    <th>User Balance</th>
                                                    <th>Actions</th>
                                                </tr>
                                                <tbody>
                                           
                                                    {activeRecords.map(record => {
                                                    
                                                        return (
                                                       <RecordTable recordData={record}/>
                                                        )
                                                     
                                                     })} 
                                                </tbody>
                                            </Table>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                     )
            }else{
                return (
                    <Jumbotron>
                        <h1>There are no records on offer as of the moment.</h1>
                    </Jumbotron>
                )
            }
    }
} 

//pre-fetch data for pre-rendering our component
//use getServerSideProps so that the fetch request will be sent to the API on every request sent to this page

