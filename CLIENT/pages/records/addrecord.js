import { useState, useEffect, useContext } from 'react'
import { Form, Alert, Jumbotron, Container, Row, Col } from 'react-bootstrap'
import {Button} from '@material-ui/core'
import Router from 'next/router'
import UserContext from '../../UserContext'

// export async function getServerSideProps() {
//     const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`)
//     const data = await res.json()


//     return {
//         props: {
//             data
//         }
//     }
// }



export default function addrecord() {
    const { user, categories, setCategories, balance, setBalance, records, setRecords } = useContext(UserContext)
    const [description, setDescription] = useState('')
    const [ amount, setAmount] = useState(0)
    const [categoryType, setCategoryType] = useState('income')
    const [categoryName, setCategoryName] = useState('')
    const [activeCategories, setActiveCategories] = useState([])
    const [isActive, setIsActive] = useState(false)
    const [notify, setNotify] = useState(false)

	useEffect(() => {
        if(description.length < 50){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [description])

    console.log(balance)

    function add(e){
        e.preventDefault()

        const computeBalance = function(){
        	if(categoryType === 'income'){
        		return +balance + +amount  
        	} else{
        		return +balance - +amount 
        	}
        }

            console.log(balance, amount)
        	const newBalance = computeBalance()
        	
        
        console.log(newBalance, "newBalance")
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/records`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                categoryType: categoryType,
                description: description,
                amount:amount,
                runningBalance: newBalance 
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data===false){
                setNotify(true)
            }else{
                fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}`,{
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}` 
                },
                body: JSON.stringify({
                   balance: newBalance
                })
            })
                .then(res => res.json())
                .then(data => {
                console.log(data)
                setBalance(data.balance) 
                // Router.push('/records')
                })
            }
        })
    }

    console.log(records)
         return (
            <>
            <Container fluid>
                <Row>
                    <Col xs={12} id="add-record-first-layer">
                        <Row>
                            <Col xs={12} md={{ span: 6, offset: 3 }}   id="add-record-second-layer">
                                <Form onSubmit={(e) => add(e)}>
                                    <Form.Group >
                                        <Form.Label className="text-white">Category Name:</Form.Label>
                                        	<Form.Control type="text" onChange={e => setCategoryName(e.target.value)} as="select" custom required>
                                      	<option value="">choose below</option>
                                        {categories.map(category => { 
                                        	return(
                                            <option key={category._id} value={category.name}>{category.name}</option>
                                            )
                                            })
                                                            }
                                        </Form.Control>
                                    </Form.Group> 
                                    <Form.Group >
                                        <Form.Label className="text-white">Category Type:</Form.Label>
                                        	<Form.Control type="text"  onChange={e => setCategoryType(e.target.value)} as="select" custom required>
                                        <option value="income">income</option>
                                        <option value="expense">expense</option>
                                        </Form.Control>
                                    </Form.Group>
                                    
                                    <Form.Group>
                                        <Form.Label className="text-white">Description:</Form.Label>
                                        <Form.Control type="text" onChange={e => setDescription(e.target.value)} />
                                        {description.length >= 50 ? <Alert variant="warning">description has exceeded maximum length</Alert>:null}
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label className="text-white">Amount:</Form.Label>
                                        <Form.Control type="number" onChange={e => setAmount(e.target.value)} required />
                                    </Form.Group>
                                    {isActive===true
                                    ? <Button type="submit" variant="contained" color="primary">add</Button>
                                    : <Button disabled type="submit" variant="contained" color="primary">add</Button>}
                                </Form>

                                {notify === true
                                ? <Alert variant="danger">Failed to create a Category!</Alert>
                                : null}
                            </Col>
                        </Row>
                    </Col>
                </Row>    
            </Container>
            </>
        )

   

}

