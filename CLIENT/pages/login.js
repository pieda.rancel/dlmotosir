
import { useState, useEffect, useContext } from 'react'
import { Form,Container, Row, Col, Button } from 'react-bootstrap'
import {FormControl, InputLabel, FilledInput, FormGroup, TextField} from '@material-ui/core';
import UserContext from '../UserContext'
import Router from 'next/router'
import { GoogleLogin } from 'react-google-login' 
import Swal from 'sweetalert2'


export default function login() {
    //use the UserContext and destructure it to obtain the setUser state setter from our application entry point
    const { setUser } = useContext(UserContext)
    //states for form input
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    //send fetch request to the API endpoint responsible for user authentication
    function authenticate(e){
        //prevent redirection via form submission 
        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })  
        .then(res => res.json())
        .then(data => {
           //successful authentication will return a JWT
           console.log(data)
            if(data.accessToken){
                //store this JWT in local storage
                localStorage.setItem('token', data.accessToken)
                //obtain user id and isAdmin properties for setting in our globally scoped user 
                //state by sending a fetch request to the appropriate API endpoint
                fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}` 
                    }
                })
                .then(res => res.json())
                .then(data => {
                    //set the global user state to have the proper id and isAdmin
                    setUser({
                        id:data._id
                    })

                    Router.push('/categories/addcategory')
                })
            }else{
                //authentication failure, redirect to error page
                Router.push('/error')
            }
        })
    }

    const captureLoginResponse = (response) =>{
        console.log(response)
       const payload = {
        method: 'post',
        headers:{'Content-Type': 'application/json'},
        body: JSON.stringify({tokenId: response.tokenId})
       }
       fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verify-google-id-token`, payload)
       .then((response)=> response.json())
       .then(data =>{
            //if the data return by the api has an accesstoken, we'll set the localstorage
            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                setUser({id: data._id, isAdmin:data.isAdmin})
                Router.push('/categories')
            } else {
                //else, if the data return by the api is an error msg
                if(data.error == 'google-auth-error'){
                    //error == 'google auth error', return a Swal
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed.',
                        'error'
                        )
                } else if(data.error === 'login-type-error'){
                    //error == 'login type error, return a Swal'
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure.',
                        'error'
                        )
                }
            }
            // if the data return by the api has an accesstoken, we'll set the 
            // localstorage, else, if the data return by the api is an error msgs
            //     error == 'google auth error', return a Swal
            //     error == 'login'
       })
    }


    return (
        
        <Container id="login-background" fluid>
            <Row>
                <Col xs={12} md={{ span: 6, offset: 3 }}   id="login-first-layer">
                    
                         
                         <h3 className="text-white">Sign in</h3>
                    
                    <Row>
                        <Col xs={12} id="login-second-layer">
                   
                    
                    <Form onSubmit={(e) => authenticate(e)}>
                    <Form.Group>
                     
                            <Form.Label className="text-white">Email</Form.Label>
                            <Form.Control type="email" value={email} onChange={(e)=>setEmail(e.target.value)} />
                        </Form.Group>
                    <Form.Group>
                            <Form.Label className="text-white">Password</Form.Label>
                            <Form.Control type="password" value={password} onChange={(e)=>setPassword(e.target.value)} />
                    </Form.Group>  
                        <Button variant="success" type="submit">Submit</Button>
                    </Form>
                    <p className="text-white">or</p>
                   
                    <GoogleLogin clientId='719590584713-gn89bo3715u1ibnoc2fpm7k7avlem4f4.apps.googleusercontent.com' 
                    onSuccess={captureLoginResponse} 
                    onFailure={captureLoginResponse} 
                    cookiePolicy={'single_host_origin'}
                    render={renderProps=>(
                        <Button variant="outline-success" onClick={
                            renderProps.onClick} disabled={
                                renderProps.disabled}>This is my custom
                                Google button</Button>
                        )}/>
                        </Col>
                    </Row>
                </Col>
            </Row>    
        </Container>
        
        
    )
}
// <span><i className="fas fa-lock fa-3x "></i></span>
