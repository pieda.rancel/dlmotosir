import Head from 'next/head'
import {Container,Row, Col} from 'react-bootstrap'

export default function Home() {
  return (
    <>
      <Head>
        <title>Budget Tracker</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
    <Container id="landing-page-background" fluid>
      <Row>
        <Col xs={12} md={{ span: 6, offset: 3 }} id="landing-first-layer">
          <Row>
            
          </Row>
            
        </Col>
      </Row> 
    </Container>
    </>
  )
}
