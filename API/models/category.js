const mongoose = require('mongoose')

const categorySchema = mongoose.Schema({
	name:{ 
		type: String,
		required:[true, "Category Name is required"]
	},
	isActive:{ 
		type:Boolean,
		default:true
	},
	type:{ 
		type:String,
		required:[true, "Category type is required"]
	},
	users: 
	[
		{
			userId:{		
				type: String,
				required: [true, 'User ID is required!']
			}
		}
	], 
	records:
	[
		{
			recordId:{
				type:String,
				required:[true, "Record id is required"]
			},
			createdOn:{
				type:Date,
				default:new Date()
			} 

		}
	]
})

module.exports = mongoose.model('category', categorySchema)