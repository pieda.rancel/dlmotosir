const mongoose = require('mongoose')

const accountSchema = mongoose.Schema({
	bankName:{
		type: String,
		required:[true, "Category Name is required"]
	},
	amount:{
		type: Number,
		default:0
	},
	accountType:{
		type: String,
		required:[true, "Category Name is required"]
	},
	userId: {
			type: String,
			required: [true, 'User ID is required!']
		}
	
})

module.exports = mongoose.model('account', accountSchema)