const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
	email:{
		type: String,
		required:[true, "email is required"]
	},
	password:{
		type:String,
		// required:[true, "password is required"]
	},
	createdOn:{
		type:Date,
		default:new Date()
	},
	loginType:{
		type:String,
		// required: [true, "Login type is required"]
	},
	categories:
	[
		{
			categoryId:{
				type:String,
				required:[true, "Category id is required"]
			}
		}
	],
	balance:{
		type:Number,
		default:0
	},
	accounts:
	[
		{
			accountId:{
				type:String,
				required:[true, "Account id is required"]
			},
			createdOn:{
				type:Date,
				default:new Date() 
			}
		}
	],
	records:
	[
		{
			recordId:{
				type:String,
				required:[true, "Record id is required"]
			},
			createdOn:{
				type:Date,
				default:new Date()
			} 
			
		}
	]
})

module.exports = mongoose.model('user', userSchema)	