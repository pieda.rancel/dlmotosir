const Category = require('../models/category')
const User = require('../models/user')

module.exports.add = (params) =>{
	const category = new Category({
		name:params.name,
		type:params.type
	}) 

	return category.save()
	.then((category, err)=>{
		if(err){
			 return false
		
		}else{ 
		return	User.findById(params.userId).then((user, err)=>{		
				if (err){ 
					return false
					
				}else{
					user.categories.push({
						categoryId:category._id
					})
				return user.save().then((updatedUser, err)=>{ 
					if (err){ 
						return false

					}else{ 
						category.users.push({
							userId:updatedUser._id
						})
						return category.save().then((updatedCategory,err)=>{ 
							return (err) ? false : true
						})
					} 
				})
				}
			})
			 
		}
	})
} 

//retrieve all categories of a user
module.exports.getAll = ()=>{
	return Category.find().then(categories => categories)  
}

module.exports.getOne = (params)=>{
	return Category.findById(params.id)
	.then((category, err)=>{
		if (err) return false
		return category
	})
}

module.exports.update = (params)=>{
	return Category.findById(params.categoryId)
	.then((category, err)=>{
		console.log(category)
		if (err) return false
		category.name = params.name
		category.type = params.type
	return category.save()
	.then((updatedCategory, err) => {
            return (err) ? false : true
        })

	})
}

module.exports.archive = (params)=>{
	return Category.findById(params.id)
	.then((category, err)=>{
		if (err) return false
	if(category.isActive === true){
		category.isActive = false
	}else{
        category.isActive = true
    }
	return category.save()
	.then((updatedCategory, err) => {
                return err ? false : true
            })

	})
}

