
const Account = require('../models/account');
const User = require('../models/user');

//function for creating a new account
module.exports.createAccount = params => {
	const account = new Account ({
		bankName: params.bankName,
		amount: params.amount,
		accountType: params.accountType,
		userId: params.userId
	});
	
	//save the newly account in the database
	return account.save().then((account, error) => {
		if(error) return false
		
		//process for pushing the newly created account into the user
		else{
			//find the user record in the database
			return User.findById(params.userId).then((user, error) => {
				if(error) return false
				
				//push the new account id in the user's array of account IDs
				else{
					user.accounts.push({
						accountId: account._id
					});

					return user.save().then((updatedUser, error) => {
						return (error) ? false : true
					});
				}
			})
		}
	});
}

//function for retrieving all the accounts
module.exports.getAccounts = params => {
	return Account.find({userId: params.userId}).then(accounts => accounts);
}

//function for finding a single account
module.exports.findAccount = params => {
	return Account.findById(params.id).then((account, error) => {
		return (error) ? false : account
	});
}





//function for deleting an account
module.exports.deleteAccount = params => {
	//look for the user document in the database
	return User.findById(params.userId).then((user, error) => {
		if (error) return false

		else{
			//filter out the account ID to be deleted from the user's array of account IDs
			const newAccountsArray = user.accounts.filter(account => params.accountId === account.accountId);
			
			user.accounts = newAccountsArray;
			
			//update the user document
			return user.save().then((updatedUser, error) => {
				if(error) return false

				else{
					//delete all the posts related to the account to be deleted
					return Post.deleteMany({accountId: params.accountId}).then((posts, error) => {
						if(error) return false

						else{
							//delete the account from the database
							return Account.deleteOne({_id: params.accountId}).then((updatedAccount, error) => {
								return (error) ? false : true
							});	
						}
					});
				}
			});
		}
	});
}