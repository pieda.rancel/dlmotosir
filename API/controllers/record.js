const Record = require('../models/record')
const User = require('../models/user')
const Category = require('../models/category')

module.exports.addRecord = (params)=>{
	const record = new Record({
		categoryName: params.categoryName,
		categoryType: params.categoryType,
		amount: params.amount,
		runningBalance: params.runningBalance, 
		description: params.description
	})

	return record.save().then((record, err)=>{
		if (err){
			return false
		}else{
		return User.findById(params.userId).then((user, err)=>{
			user.records.push({
				recordId: record._id
			})
			return user.save().then((updatedUser, err)=>{
				if (err){
						return false 
				}else{
					record.users.push({
						userId: updatedUser._id
					})
				
					return record.save().then((updatedRecord, err)=>{ 
						if(err) return false
						return Record.find().then(records => records) 
					})
				}
			})
		})

		} 
	})
}

module.exports.getAll = ()=>{
	return Record.find().then(records => records)
}

module.exports.getUserRecords = (params)=>{
	return User.findById(params.id).then((user, err)=>{
		if (err) return null
		const recordIds = user.records.map(record => {
			return record.recordId
		})
		console.log(recordIds)
		if (recordIds.length > 0){ 
			return Record.find().where('_id').in(recordIds).then((records, err) => {
					if (err) return null	
				console.log(records, "kahit anu")
					return records
			});
		}else{
			return false 
		}
	})
} 

module.exports.getAllIds = () => {
    return Record.find()
    .then((records, err) => {
        if (err) return false
        const ids = records.map(record => record._id)
        return ids
    })
}

// module.exports.archive = (params)=>{
// 	return Record.findById(params.id)
// 	.then((record, err)=>{
// 			if (err) return false
// 		if(record.isActive === true){
// 			record.isActive = false
			
// 		}else{
// 	        record.isActive = true
	      
// 	    }
// 		return record.save() 
// 		.then((updatedRecord, err) => {
// 	      	return (err) ? false : true

// 	    })


// 	})
// }

module.exports.toggleDelete = (params) => {
	return User.findById(params.userId).then((user, error) => {
		console.log(user)
		if(error) return false
		return Record.findById(params.recordId).then((record, error) => {
			// console.log(record, "record")
			if(record.isActive === true){
				record.isActive = false
				if(record.categoryType === 'expense'){
					user.balance = +user.balance + +record.amount
				} else {
					user.balance = +user.balance - +record.amount
				}
			} else {
				record.isActive = true
				if(record.categoryType === 'expense'){
					user.balance = +user.balance - +record.amount
				} else {
					user.balance = +user.balance + +record.amount
				}
			}
			return record.save().then((updatedRecord, error) => {
				// console.log(updatedRecord, "updatedRecord")
				if(error) return false
					return user.save().then((user, error) => {
						if(error) return false
						console.log("kahit anu")
						return user
					})
			})
		})
	})
}
