const express = require('express')
const router = express.Router()
const CategoryController = require('../controllers/category')
const auth = require('../auth')

router.post('/', auth.verify, (req, res)=>{
		const params = {
			userId: auth.decode(req.headers.authorization).id,
			name: req.body.name,
			type: req.body.type
		}
	CategoryController.add(params).then(result => res.send(result))
})

router.get('/', (req, res)=>{ 

	CategoryController.getAll().then(result => res.send(result))
})

router.get('/:id', (req, res) => {
    CategoryController.getOne({id: req.params.id}).then(result => res.send(result))
})

router.put('/:categoryId', auth.verify, (req, res)=>{
	const params = {
		name: req.body.name,
		type: req.body.type,
		categoryId: req.params.categoryId
	}
	CategoryController.update(params).then(result=>res.send(result))
})

router.delete('/:id', auth.verify, (req, res)=>{
	const params = {
		id: req.params.id
	}
	CategoryController.archive(params).then(result=>res.send(result))
})

module.exports = router

