const express = require('express') 
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req, res)=>{
	UserController.login(req.body).then(result=>res.send(result))
})

//route for getting user info from decoded JWT payload
router.get('/details', auth.verify, (req, res) => {
    //user will have an id, email, and isAdmin properties
    const user = auth.decode(req.headers.authorization)
    //pass in the user id property to a controller function for retrieving all user information
    UserController.getDetails({userId: user.id}).then(result => res.send(result))
})

router.get('/ids', (req, res) => {
    UserController.getAllIds().then(result => res.send(result))
})

router.get('/:id', (req, res) => {
    UserController.getDetails({userId: req.params.id}).then(result => res.send(result))
})

router.get('/:id/categories', (req, res) => {
    UserController.getAll(req.params.id).then(result => res.send(result))
})

router.patch('/:id', (req, res) => {
	const params = {
		id:req.params.id,
		balance: req.body.balance
	}
    UserController.editBalance(params).then(result => res.send(result))
})

router.post('/verify-google-id-token', async (req, res)=> {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

router.get('/:userId/records', (req, res)=>{

    UserController.getUserRecords(req.params).then(result=>res.send(result))
})


module.exports = router