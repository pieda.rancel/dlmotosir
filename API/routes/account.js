const express = require('express');
const router = express.Router();

const AccountController = require('../controllers/account');
const auth = require('../auth');

//route for creating a new account
router.post('/', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		bankName: req.body.bankName,
		amount: req.body.amount,
		accountType: req.body.accountType
	}

	AccountController.createAccount(params).then(result => res.send(result));
});

//route for retrieving all the accounts of the user
router.get('/', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id
	}

	AccountController.getAccounts(params).then(result => res.send(result));
});

//route for retrieving a single account
router.get('/:id', (req, res) => {
	AccountController.findAccount({id: req.params.id}).then(result => res.send(result));
});

//route for deleting an account
router.delete('/:accountId', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		accountId: req.params.accountId
	}

	AccountController.deleteAccount(params).then(result => res.send(result));
});

module.exports = router;