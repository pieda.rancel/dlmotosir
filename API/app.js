const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
require('dotenv').config()
const port = process.env.PORT
const corsOptions = {
	origin:["http://localhost:3000", 
		"https://budget-tracker-app-ten.vercel.app", 
		"https://budget-tracker-app-git-master.jaysonguyong.vercel.app",
		"https://budget-tracker-app-nm1qxjljj.vercel.app",
		"https://budget-tracker-app-jo2b7c3m5.vercel.app"
	],
	optionsSuccessStatus: 200
}

mongoose.connect(process.env.MONGODB, {
	useNewUrlParser:true,
	useUnifiedTopology:true,
	useFindAndModify:false
})

db= mongoose.connection;

db.on('error', ()=>{
	console.error(`Oops something went wrong in the MongDB connection`)	
})

db.once('open',()=>{
	console.log(" Now connected to the MongoDB Server")	
})

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.listen(process.env.PORT, ()=>{
	console.log(`Now listening to port ${port}`)
})

const userRoutes = require('./routes/user')
const categoryRoutes = require('./routes/category')
const accountRoutes = require('./routes/account')
const recordRoutes = require('./routes/record')


app.use('/api/users', cors(corsOptions), userRoutes)
app.use('/api/categories', cors(corsOptions), categoryRoutes)
app.use('/api/accounts', cors(corsOptions), accountRoutes)
app.use('/api/records', cors(corsOptions), recordRoutes)
