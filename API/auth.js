const jwt = require('jsonwebtoken');
const secret = process.env.SECRET;


module.exports.createAccessToken = user => {
	
	const data = {
		id: user._id,
		email: user.email
	}

	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			return (error) ? res.send({auth: 'failed'}) : next()
		});
	}

	else res.send({auth: 'failed'});
}

module.exports.decode = token => {
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length);

		return jwt.decode(token, secret, (error, data) => {
			return (error) ? null : jwt.decode(token, {complete: true}).payload 
		});
	}

	else return null
}

